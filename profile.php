<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require 'classes/bootstrap.php';
$cfg = parse_ini_file('classes/config.ini', true);
//Process form sumbission
if(!empty($_POST))
{
    $error = false;
    $messages = '';
    //Check old password
    if($_POST['old_pswd'])
    {
        $check = \Sart\User::model()->findByAttributes([
            'email'=>$user->email,
            'password'=>md5($_POST['old_pswd'])
        ]);
        if(!$check)
        {
            $error = true;
            \Sart\Alert::flashError('Invalid old password!');        
        }else{
        
            if($_POST['new_pswd'] !== $_POST['confirm_pswd'])
            {
                $error = true;
                \Sart\Alert::flashError('Password and Password Confirmation do not match!');        
            }else{
                $user->password = md5($_POST['new_pswd']);
            }
        }
        
    }
    
    if($_POST['firstname']) {
        $user->firstname = htmlspecialchars(strip_tags($_POST['firstname']));
    }

    if($_POST['lastname']) {
        $user->lastname = htmlspecialchars(strip_tags($_POST['lastname']));
    }

    if($_POST['license']) {
        if ( $auth->verifyLicense($user->email, htmlspecialchars(strip_tags($_POST['license'])), 'pro') ) {
            $user->license = htmlspecialchars(strip_tags($_POST['license']));
            $user->account_type = 1;
        } elseif ($auth->verifyLicense($user->email, htmlspecialchars(strip_tags($_POST['license'])))) {
            $user->license = htmlspecialchars(strip_tags($_POST['license']));
            $user->account_type = 0;
        } else {
            $user->license = null;
        }
    }

    if($_POST['license_vip'] && $_POST['license_vip'] != '') {
        if ($auth->verifyLicense($user->email, htmlspecialchars(strip_tags($_POST['license_vip'])), 'vip')) {
            $user->license_vip = htmlspecialchars(strip_tags($_POST['license_vip']));
            $user->account_type = 3;
            if (!$user->license_vip_startdate) $user->license_vip_startdate = date('Y-m-d');
            $user->license_vip_lastcheckup = date('Y-m-d');
        } else {
            $user->license_vip = null;
        }
    }
	if($_POST['characters_code'] && $_POST['characters_code'] != '') {
        	
			
			$characters_code = trim($_POST['characters_code']);
			$url = "http://softwarelicensing.pro/licenses/a?action=check_license&license=".$characters_code ."&product_number=360675&slug=sketch-maker-pro-credits";
			$cURLConnection = curl_init();
			curl_setopt($cURLConnection, CURLOPT_URL, $url );
		
			curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($cURLConnection);
			curl_close($cURLConnection);
			$json_decode = json_decode($response,true);
			if( $json_decode['status'] == "valid" ){
				if($user->updateCharacterLimit( $user->id ,  $cfg['render']['max_characters'] , $_POST['characters_code'] )){
					\Sart\Alert::flashSuccess('Character limit has been updated successfully!');   
				}else{
					\Sart\Alert::flashError('Error! You have already used this code');
				}
			}else{
				\Sart\Alert::flashError('Error! Invalid code');
			}
			
		
    }
    if(!$error) {
        if($user->save()) {
            \Sart\Alert::flashSuccess('User info has been updated!');            
        } else {
            \Sart\Alert::flashError('Error during update user info!');            
        }
    }
    

}

$sort = get_sort();

$videos = \Sart\Video::getVideosForGrid($user->id,$sort['order'] );



//Some service variables
//@todo: move to class
$page_title = 'Profile';
$current_page = 'profile';

?><!doctype html>
<html lang="en">
<head>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-52577223-14"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-52577223-14');
</script>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?php echo $page_title; ?></title>

  <link href="vendor/twbs/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
  <link href="vendor/fortawesome/font-awesome/css/font-awesome.css" rel="stylesheet">
  <link href="css/main.css" rel="stylesheet">
</head>

<body class="">
<?php
include_once('includes/navbar.php');
	$characters_codes = '';
?>

<div class="containe appContainer">
    <div class="row">
        <main role="main" class="col-12">
            <h1>Profile</h1>
            <a href="index.php" class="btn btn-primary mb-3 mt-2">New Project</a>
<?php
    \Sart\Alert::render();
?>            
            <form method="post" id="profile_form" class="card card-body bg-faded" autocomplete="off">
                <div class="row mb-4">
                    <div class="col-sm-5 ">
                        <fieldset>
                            <legend>License</legend>
                            <div class="form-group">
                                <label>License</label>
                                <input type="text" class="form-control" name="license" value="<?php echo $user->license; ?>" placeholder="add license code here"/>
                            </div>
                            <div class="form-group">
                                <label>VIP License</label>
                                <input type="text" class="form-control" name="license_vip" value="<?php echo $user->license_vip; ?>" placeholder="add license code here"/>
                            </div>
							<div class="form-group">
                                <label>Add more text Characters</label>
                                <input type="text" class="form-control" name="characters_code" id="characters_code" value="<?php echo $characters_codes;?>"  placeholder="SL-3508148696STS"/>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Login Info</legend>
                            <div class="form-group">
                                <label>Email(login)</label>
                                <input type="email" disabled class="form-control" name="" value="<?php echo $user->email; ?>" placeholder="Email"/>
                            </div>                            
                            <div class="form-group">
                                <label>Old Password</label>
                                <input type="password" class="form-control" name="old_pswd" value="" placeholder="Old password"/>
                            </div>
                            <div class="form-group">
                                <label>New Password</label>
                                <input type="password" id="password" class="form-control" name="new_pswd" value="" placeholder="New Password"/>
                            </div>
                            <div class="form-group">
                                <label>Confirm Password</label>
                                <input type="password" id="confirm_password" class="form-control" name="confirm_pswd" value="" placeholder="Confirm Password"/>
                            </div>                             
                        </fieldset>                        
                    </div>
                    <div class="col-sm-5 offset-sm-2">
                        <fieldset>
                            <legend>Personal Info</legend>
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" class="form-control" name="firstname" value="<?php echo $user->firstname; ?>" placeholder="First Name" required/>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" class="form-control" name="lastname" value="<?php echo $user->lastname; ?>" placeholder="Last Name" required/>
                            </div>
                        </fieldset>

                        <fieldset>
                            <?php
                            if ($user->isAdmin || $user->account_type == 3) {
                                echo "<legend>Quota</legend>";
                            } else if ($user->account_type == 1) {
                                echo "<legend>Quota - <a href=\"http://sketchmakerpro.com/upgrade-gui-2-pp.html\">UPGRADE account</a></legend>";
                            } else {
                                echo "<legend>Quota - <a href=\"http://sketchmakerpro.com/upgrade-gui-1.html\">UPGRADE account</a></legend>";
                            }
                            ?>
                            <div id="quotasDisplayContainer">
                                <div class="container">
                                    <label>Videos per hour quota (<span id="quotaVideo_perhour_label">0 / 0</span>)</label>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 0%" id="quotaVideo_perhour">
                                            <span id="quotaVideo_perhour_title">0% Used</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <label>Queued Videos for next hour (<span id="quotaVideo_queue_label">0 / 0</span>)</label>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 0%" id="quotaVideo_queue">
                                            <span id="quotaVideo_queue_title">0% Used</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <label>Total video quota (<span id="quotaVideo_total_label">0 / 0</span>)</label>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 0%" id="quotaVideo_total">
                                            <span id="quotaVideo_total_title">0% Used</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <label>Disk space (<span id="quota_filesize_label">0 / 0</span>)</label>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 0%" id="quota_filesize">
                                            <span id="quota_filesize_title">0% Used</span>
                                        </div>
                                    </div>
                                </div>
								
								<div class="container">
                                    <label>Characters Left <?php echo $char_info['left_character']?>/<?php echo $char_info['total_char']?> (<?php echo $char_info['left_percentage'];?>%) 
										<?php
			echo"<a target=\"_blank\"  href='https://www.sketchmakerpro.com/get-tts-credits-1.html' target='_blank'>- Get More Now --</a>";
	?>  </label>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: <?php echo $char_info['left_percentage'];?>%" id="quota_filesize">
                                            <span id="quota_filesize_title"><?php echo $char_info['left_percentage'];?>% Left</span>
                                        </div>
                                    </div>
                                </div>
								
								
                            </div>
                        </fieldset>

                        <div id="audioAdditionalInfo" class="text-center"></div>
                        <div id="videoGenerationProgress" class="videoGenerationProgress">
                            <button class="btn btn-primary btn-block"></button>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-sm-2 offset-sm-5">
                        <button type="submit" class="btn btn-primary btn-block btn-lg"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>
            </form>    
        </main>
    </div>
</div>
<?php
include_once('includes/footer.php');
?>
<div class="hide" id="sink"></div>

<script src="vendor/components/jquery/jquery.js"></script>
<script src="vendor/twbs/bootstrap/assets/js/vendor/popper.min.js"></script>
<script src="vendor/twbs/bootstrap/dist/js/bootstrap.js"></script>
<script type="text/javascript">
var password = document.getElementById("password"),
    confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value !== confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;

$(document).ready(async () => {
  try {
    const req = await fetch('api/getLimits.php');
    let limits = await req.json();

    if (limits.hasOwnProperty('error')) throw limits.error;

    if (limits.data.hasOwnProperty('video') && limits.data.video.hasOwnProperty('per_hour')) {
      if (limits.data.video.per_hour.percent >= 75) $('#quotaVideo_perhour').addClass('bg-danger');
      $('#quotaVideo_perhour').width( limits.data.video.per_hour.percent + '%');
      $('#quotaVideo_perhour_title').html( limits.data.video.per_hour.percent >= 20 ? limits.data.video.per_hour.percent + '% Used' : '');
      $('#quotaVideo_perhour_label').html( limits.data.video.per_hour.count + ' / ' + (limits.data.video.per_hour.limit || 0));
    }

    if (limits.data.hasOwnProperty('video') && limits.data.video.hasOwnProperty('queue')) {
      if (limits.data.video.queue.percent >= 75) $('#quotaVideo_queue').addClass('bg-danger');
      $('#quotaVideo_queue').width( limits.data.video.queue.percent + '%');
      $('#quotaVideo_queue_title').html( limits.data.video.queue.percent >= 20 ? limits.data.video.queue.percent + '% Used' : '');
      $('#quotaVideo_queue_label').html( limits.data.video.queue.count + ' / '+ limits.data.video.queue.limit || 0);
    }

    if (limits.data.hasOwnProperty('video') && limits.data.video.hasOwnProperty('total')) {
      if (limits.data.video.total.percent >= 75) $('#quotaVideo_total').addClass('bg-danger');
      $('#quotaVideo_total').width( limits.data.video.total.percent + '%');
      $('#quotaVideo_total_title').html( limits.data.video.total.percent >= 20 ? limits.data.video.total.percent + '% Used' : '');
      $('#quotaVideo_total_label').html( limits.data.video.total.count + ' / ' + (limits.data.video.total.limit || 0));
    }

    if (limits.data.hasOwnProperty('diskspace')) {
      if (limits.data.diskspace.percent >= 75) $('#quota_filesize').addClass('bg-danger');
      $('#quota_filesize').width( limits.data.diskspace.percent + '%');
      $('#quota_filesize_title').html( limits.data.diskspace.percent >= 20 ? limits.data.diskspace.percent + '% Used' : '');
      $('#quota_filesize_label').html( Math.round(limits.data.diskspace.count/1024) + 'Mb / ' + Math.round((limits.data.diskspace.limit || 0)/1024) + 'Mb');
    }

  } catch (e) {
    console.log(e);
  }
})
</script>
</body>
</html>
