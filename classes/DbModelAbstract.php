<?php
namespace Sart;

use spir1donov\Database;

abstract class DbModelAbstract extends ModelAbstract
{

    protected $db;
        
    abstract public function getTableName();
    
    abstract protected function getPk();
    
    public function __construct(\PDO $db = null) {
        if($db) {
            $this->db = $db;
        } else {
            $this->db = Database::getDb();
        }
        
    }
    
    public function findByPk($value) {
        $attr = $this->getPk();
        $where[] = " `$attr` = :$attr";
        $values[$attr] = $value;
        
        $query = $this->db->prepare("SELECT *  FROM `{$this->getTableName()}` WHERE ". implode(' AND ', $where)." LIMIT 1");

        
        $query->execute($values);
        
        $this->_attributes = $query->fetch();
        
        return !empty($this->_attributes) ? $this : null;        
    }

    public function calcLimits($user) {
        $limits = (object)array();

        $limits->video = (object)array();
        $limits->video->pro = (object)array();
        $limits->video->pro->per_hour = (object)array();
        $limits->video->pro->total = (object)array();
        $limits->video->pro_plus = (object)array();
        $limits->video->pro_plus->per_hour = (object)array();
        $limits->video->pro_plus->total = (object)array();
        $limits->video->basic = (object)array();
        $limits->video->basic->per_hour = (object)array();
        $limits->video->basic->total = (object)array();
        $limits->diskspace = (object)array();
        $limits->diskspace->pro = (object)array();
        $limits->diskspace->pro_plus = (object)array();
        $limits->diskspace->basic = (object)array();
        $limits->slides = (object)array();
        $limits->slides->pro = (object)array();
        $limits->slides->pro_plus = (object)array();
        $limits->slides->basic = (object)array();
        $limits->background_images = (object)array();
        $limits->background_images->pro = (object)array();
        $limits->background_images->pro_plus = (object)array();
        $limits->background_images->basic = (object)array();
        $limits->music_files = (object)array();
        $limits->music_files->pro = (object)array();
        $limits->music_files->pro_plus = (object)array();
        $limits->music_files->basic = (object)array();
        $limits->character_limit = (object)array();
        $limits->character_limit->pro = (object)array();
        $limits->character_limit->pro_plus = (object)array();
        $limits->character_limit->basic = (object)array();

        $query = $this->db->prepare("SELECT `object`,`key`,`value`  FROM `settings` WHERE `type`=10");
        $query->execute();
        $res = $query->fetchAll();

        foreach ($res as $limit) {
            if ($limit['key'] == "video_per_hour") {
                if ($limit['object'] == 1) { #pro
                    $limits->video->pro->per_hour->limit = $limit['value'];
                } else if ($limit['object'] == 3) {
                    $limits->video->pro_plus->per_hour->limit = $limit['value'];
                } else { #basic
                    $limits->video->basic->per_hour->limit = $limit['value'];
                }
            } elseif ($limit['key']  == "video_total") {
                if ($limit['object'] == 1) { #pro
                    $limits->video->pro->total->limit = $limit['value'];
                } else if ($limit['object'] == 3) {
                    $limits->video->pro_plus->total->limit = $limit['value'];
                } else { #basic
                    $limits->video->basic->total->limit = $limit['value'];
                }
            } elseif ($limit['key']  == "diskspace") {
                if ($limit['object'] == 1) { #pro
                    $limits->diskspace->pro->limit = $limit['value'];
                } else if ($limit['object'] == 3) {
                    $limits->diskspace->pro_plus->limit = $limit['value'];
                } else { #basic
                    $limits->diskspace->basic->limit = $limit['value'];
                }
            }
        }

        $query = $this->db->prepare("SELECT `object`,`key`,`value`  FROM `settings` WHERE `type`=11");
        $query->execute();
        $res = $query->fetchAll();

        foreach ($res as $limit) {
            if ($limit['key'] == "slides") {
                if ($limit['object'] == 1) { #pro
                    $limits->slides->pro->limit = $limit['value'];
                } else if ($limit['object'] == 3) {
                    $limits->slides->pro_plus->limit = $limit['value'];
                } else { #basic
                    $limits->slides->basic->limit = $limit['value'];
                }
            }
        }

        $query = $this->db->prepare("SELECT `object`,`key`,`value`  FROM `settings` WHERE `type`=12");
        $query->execute();
        $res = $query->fetchAll();

        foreach ($res as $limit) {
            if ($limit['key'] == "background_images") {
                if ($limit['object'] == 1) { #pro
                    $limits->background_images->pro->limit = $limit['value'];
                } else if ($limit['object'] == 3) {
                    $limits->background_images->pro_plus->limit = $limit['value'];
                } else { #basic
                    $limits->background_images->basic->limit = $limit['value'];
                }
            }
        }

        $query = $this->db->prepare("SELECT `object`,`key`,`value`  FROM `settings` WHERE `type`=13");
        $query->execute();
        $res = $query->fetchAll();

        foreach ($res as $limit) {
            if ($limit['key'] == "music_files") {
                if ($limit['object'] == 1) { #pro
                    $limits->music_files->pro->limit = $limit['value'];
                } else if ($limit['object'] == 3) {
                    $limits->music_files->pro_plus->limit = $limit['value'];
                } else { #basic
                    $limits->music_files->basic->limit = $limit['value'];
                }
            }
        }

		$query = $this->db->prepare("SELECT `object`,`key`,`value`  FROM `settings` WHERE `type`=14");
        $query->execute();
        $res = $query->fetchAll();

        foreach ($res as $limit) {
            if ($limit['key'] == "character_limit") {
                if ($limit['object'] == 1) { #pro
                    $limits->character_limit->pro->limit = $limit['value'];
                } else if ($limit['object'] == 3) {
                    $limits->character_limit->pro_plus->limit = $limit['value'];
                } else { #basic
                    $limits->character_limit->basic->limit = $limit['value'];
                }
            }
        }

        if (isset($user->id)) {



            // count total videos
            $limits->video->total = (object)array();
            $limits->video->per_hour = (object)array();
            $limits->video->queue = (object)array();

            if ($user->isAdmin) {
                $limits->video->total->limit = 0;
                $limits->video->per_hour->limit = 0;
                $limits->video->queue->limit = 0;
                $limits->diskspace->limit = 0;
            } else {
                $query = $this->db->prepare("SELECT `key`,`value` FROM `settings` WHERE `type` = 10 and `object`= :account_type");
                $query->execute([
                    'account_type' => $user->account_type
                ]);
                $res = $query->fetchAll();
                foreach ($res as $limit) {
                    switch ($limit['key']) {
                        case "video_per_hour":
                            $limits->video->per_hour->limit = (int) preg_replace('/\D/', '', $limit['value']);
                            break;

                        case "video_total":
                            $limits->video->total->limit = (int) preg_replace('/\D/', '', $limit['value']);
                            break;

                        case "diskspace":
                            $limits->diskspace->limit = (int) preg_replace('/\D/', '', $limit['value']);
                            break;
                    }
                }
            }

            $query = $this->db->prepare("SELECT COUNT(*) as count FROM `videos` WHERE `owner` = :id LIMIT 1");
            $query->execute([
                'id' => $user->id
            ]);
            $res = $query->fetch();
            $limits->video->total->count = (int) preg_replace('/\D/', '', $res['count']);

            if (isset($limits->video->total)) {
                if ( isset($limits->video->total->limit) && $limits->video->total->limit > 0 ) {
                    $limits->video->total->percent = round(100 * $limits->video->total->count / $limits->video->total->limit, 2);
                } else {
                    $limits->video->total->percent = 0;
                }
            }


            // count last hour videos count
            $query = $this->db->prepare("SELECT COUNT(*) AS count FROM videos WHERE owner = :id AND ( created BETWEEN DATE_SUB(NOW(), INTERVAL 1 HOUR) AND NOW());" );
            $query->execute([
                'id' => $user->id
            ]);
            $res = $query->fetch();
            $limits->video->per_hour->count = (int) preg_replace('/\D/', '', $res['count']);

            if (isset($limits->video->total->limit)) {
                $limits->video->queue->limit = (int)preg_replace('/\D/', '', $limits->video->total->limit);
            } else {
                $limits->video->queue->limit = 0;
            }

            $query = $this->db->prepare("SELECT COUNT(*) AS count FROM videos WHERE owner = :id AND created > NOW();" );
            $query->execute([
                'id' => $user->id
            ]);

            $res = $query->fetch();
            $limits->video->queue->count = (int) preg_replace('/\D/', '', $res['count']);

            if (isset($limits->video->per_hour)) {
                if ( isset($limits->video->per_hour->limit) && $limits->video->per_hour->limit > 0 ) {
                    $limits->video->per_hour->percent = round(100 * $limits->video->per_hour->count / $limits->video->per_hour->limit,2);
                    if (isset($limits->video->queue)) {
                        $limits->video->queue->percent = round(100 * $limits->video->queue->count / $limits->video->per_hour->limit, 2);
                    }
                } else {
                    $limits->video->per_hour->percent = 0;
                    if (isset($limits->video->queue)) {
                        $limits->video->queue->percent = 0;
                    }
                }
            }




            // count disk space used
            $query = $this->db->prepare("SELECT COUNT(*) as count from videos where owner = :id AND created > DATE_SUB(NOW(), INTERVAL 1 HOUR)");
            $query->execute([
                'id' => $user->id
            ]);
            $res = $query->fetch();
            // TODO: !
            $f = $this->getUserDir();
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                $limits->diskspace->count = 189440;
            } else {
                $io = popen ( '/usr/bin/du -sk ' . $f, 'r' );
                $size = fgets ( $io, 4096);
                $size = substr ( $size, 0, strpos ( $size, "\t" ) );
                pclose ( $io );
                $limits->diskspace->count = $size;
            }

            if (isset($limits->diskspace)) {
                if ( isset($limits->diskspace->limit) && $limits->diskspace->limit > 0 ) {
                    $limits->diskspace->percent = round(100 * $limits->diskspace->count / $limits->diskspace->limit, 2);
                } else {
                    $limits->diskspace->percent = 0;
                }
            }
        }

        return $limits;
    }

    public function findByAttributes($attr,$value = null)
    {
        $where = [];
        $values = [];
        if(is_array($attr))
        {
            foreach($attr as $key => $val)
            {
                $where[] = " `$key` = :$key ";     
                $values[$key] = $val;
            }
        }else{
            $where[] = " `$attr` = :$attr";
            $values[$attr] = $value;
        }
        
        $query = $this->db->prepare("SELECT *  FROM `{$this->getTableName()}` WHERE ". implode(' AND ', $where)." LIMIT 1");

        
        $query->execute($values);
        
        $this->_attributes = $query->fetch();
        
        return !empty($this->_attributes) ? $this : null;
    }
    
    public function findAllByAttributes($attr,$value,$order = false) {
        $query = $this->db->prepare("SELECT *  FROM `{$this->getTableName()}` WHERE `'.$attr.'` = :attr".($order ? ' ORDER BY '.$order : ''));
        $query->execute([
            'attr'=> $value
        ]);
        $rows = $query->fetchAll();
        $models = [];
        
        foreach($rows as $row)
        {
            $model = new  get_class($this);  
            $model->setAttributes = $row;
            
            $models[] = $model;
        }
        $this->_attributes = $query->fetch();
        
        return $models;
    }
    
    public function updateAttribute($attr,$val)
    {
        $pkName = $this->getPk();
        $query = $this->db->prepare("UPDATE `".$this->getTableName()."` SET `".$attr."` = :val WHERE `{$pkName}` = :id");
        
        $query->execute([
            $pkName => $this->$pkName,
            'val' => $val
        ]);
    }

    public function getNextQueueTime($userId) {
        // "select DATE_ADD(created, INTERVAL 1 HOUR ) as next_start_datetime from videos where owner = :id AND created <= NOW() ORDER BY created DESC LIMIT 1;"
        $sql = "SELECT DATE_ADD(created, INTERVAL 1 HOUR ) as next_start_datetime FROM videos WHERE owner = :id AND ( created BETWEEN DATE_SUB(NOW(), INTERVAL 1 HOUR) AND NOW()) order by created asc limit 1;";
        $query = $this->db->prepare($sql);
        $query->execute([
            'id' => $userId
        ]);
        $res = $query->fetch();
        return $res['next_start_datetime'];
    }
    
    public function save()
    {
        $pkName = $this->getPk();
        $attrs = $this->_attributes;
        unset($attrs[$pkName]);
        $fields = [];
        $vals = [];
        if(isset($this->{$pkName}) && $this->{$pkName})
        {
            foreach($attrs as $name=>$val) {
                if ($name != 'limits') {
                    $fields[] = "`$name` = :$name";
                    $vals[$name] = $val;
                }
            }

            $query = $this->db->prepare("UPDATE `".$this->getTableName()."` SET ".implode(',',$fields)." WHERE `$pkName`=:pk" );
            $params = array_merge(['pk' => $this->$pkName], $vals);

            $result = $query->execute($params);
        }else{
            $fields['name'] = [];
            $fields['val'] = [];
            foreach($attrs as $name=>$val)
            {
              $fields['name'][] = "`$name`";
              $fields['val'][] = ":$name";
              $vals[$name] = $val;
            }
            $query = $this->db->prepare("INSERT INTO `".$this->getTableName()."` (".implode(',',$fields['name']).") VALUES(".implode(',',$fields['val']).") " );
            $result = $query->execute($vals);
            if($result)
            {
                $this->_attributes[$pkName] = $this->db->lastInsertId();
            }
        }
        
        if(!$result)
        {
            throw new \Exception($this->db->errorInfo[2]);
        }
        return true;
    }
    
    
    public function delete()
    {
        $pkName = $this->getPk();
        $query = $this->db->prepare("DELETE FROM  `".$this->getTableName()."` WHERE `$pkName`=:pk" );
        $result = $query->execute(
            [
                ':pk' => $this->$pkName,
            ]
        );
        if($result===false)
        {
            throw new \Exception($this->db->errorInfo[2]);
        }
        
        return $result;
    }
    
    
    /**
     * Check if record is new
     * @return bool
     */
    public function getIsNewRecord()
    {
        return !isset($this->{$this->getPk()});
    }
}
