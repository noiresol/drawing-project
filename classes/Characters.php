<?php
namespace spir1donov;

class Characters {
  /** @var \PDO $db */
  private $db;

  private $user;

  function __construct(\PDO $db) {
    $this->db = $db;
  }
   

  function saveCharacters($projectData) {
    $insert = $this->db->prepare("INSERT INTO `user_used_characters` (`user_id`, `project_id`, `used_character`, `added_time`) VALUES (:user_id, :project_id, :used_character, NOW() )");
  
    $insert->execute([
        'user_id' => $projectData['user_id'],
        'project_id' => $projectData['project_id'],
		'used_character' => $projectData['used_character']
    ]);
 
  }
    
}//function

