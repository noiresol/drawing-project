<?php
/**
 * Model to handle users
 * @author N. Z. <n.z@software-art.com>
 * @package CanvasSketchMaker
 */
namespace Sart;

use DateTime;
use DateInterval;

/**
 * @property object limits
 */
class User extends DbModelAbstract{
    
    /**
     * Return table name
     */
    public function getTableName()
    {
        return 'users';
    }
    
    /**
     * Return table primary key
     */
    public function getPk()
    {
        return 'id';
    }
    
    
    public function getFullName()
    {
        return $this->firstname . ' ' . $this->lastname;
    }
    
    public function getVIPMonth() {
      if ($this->_attributes['account_type'] == 0) return -1;
      if ($this->_attributes['isAdmin']) return 999;  // @TODO What to do if admin?
      $startDate = new DateTime($this->_attributes['license_vip_startdate']);
      $now = new DateTime();
      $now->setTime(0, 0, 0);
      $now->add(new DateInterval('P1D')); // Add 1 day to count first day as day 1
      $diff = $now->diff($startDate, false);
      // Month calculation is kind of janky, so at the moment assume 4 weeks = 28 days as 1 month
      return ceil($diff->days / 28);
    }
    
    /**
     * Get user dir path
     */
    public function getUserDir()
    {
        $dir = APP_PATH.DIRECTORY_SEPARATOR.'media_bin'.DIRECTORY_SEPARATOR.implode(DIRECTORY_SEPARATOR, str_split(str_pad($this->id, 12, '0', STR_PAD_LEFT), 3));
        \Sart\FileHelper::createDirIfNotExist($dir);
        return $dir;
    }
    
    /**
     * Get user url
     */
    public function getUserUrl()
    {
        $url = str_replace([APP_PATH,'media_bin'],['','storage'],$this->getUserDir());
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') $url = str_replace('\\','/', $url);
        return $url;
    }
    
    /**
     * Return path to user video directory
     */
    public function getUserVideoDir()
    {
        $dir = $this->getUserDir().DIRECTORY_SEPARATOR.'video';
        \Sart\FileHelper::createDirIfNotExist($dir);
        return $dir;
    }
    
    public function getUserVideoUrl()
    {
        return str_replace([APP_PATH,'media_bin'],['','storage'],$this->getUserVideoDir());    
    }
    
	public function setId($id)
    {
        $this->_attributes['id'] = $id;
    }
    
    
    public function toggleAdmin()
    {
        $query = $this->db->prepare("UPDATE `".$this->getTableName()."` SET isAdmin = IF(isAdmin=1, 0, 1) WHERE `id`=:pk" );
        $result = $query->execute(
            [
                ':pk' => $this->id,
            ]
        );                    
    }

    public function togglePRO($newType)
    {
        $query = $this->db->prepare("UPDATE `".$this->getTableName()."` SET account_type = :type, license = NULL, license_vip = NULL, old_account_type = :old_type, old_license = :old_license, old_license_vip = :old_license_vip WHERE `id`=:pk" );
        $result = $query->execute(
            [
                ':pk' => $this->id,
                ':type' => $newType,
                ':old_type' => $this->_attributes['account_type'],
                ':old_license' => $this->_attributes['license'],
                ':old_license_vip' => $this->_attributes['license_vip'],
            ]
        );
    }

    public function restoreLicense() {
        $query = $this->db->prepare("UPDATE `".$this->getTableName()."` SET account_type = :type, license = :license, license_vip = :license_vip, old_account_type = NULL, old_license = NULL, old_license_vip = NULL WHERE `id`=:pk" );
        $result = $query->execute(
            [
                ':pk' => $this->id,
                ':type' => $this->_attributes['old_account_type'],
                ':license' => $this->_attributes['old_license'],
                ':license_vip' => $this->_attributes['old_license_vip'],
            ]
        );
    }

    /*public function isAdmin()
    {
        $query = $this->db->prepare("SELECT isAdmin FROM `".$this->getTableName()."` WHERE `id`=:pk" );
        $result = $query->execute(
            [
                ':pk' => $this->id,
            ]
        );
    }*/

    public function getFolderLimits() {
        $query = $this->db->prepare("SELECT `id`,`min_access`,`object` FROM `limits` where `type`=1;");
        $query->execute();

        return $query->fetchAll();
    }

    public function setFolderLimits($object, $min_access) {
        $query = $this->db->prepare("INSERT INTO limits (type, object, min_access) VALUES(1, :object, :min_access) ON DUPLICATE KEY UPDATE min_access=:min_access");
        $query->execute([
            ':object' => $object,
            ':min_access' => $min_access
        ]);

        return $query->rowCount();
    }

function getUserCharacterQuota( $user_id ,$object){   
		
	  $userStmt = $this->db->prepare("SELECT sum(used_character) as used_character FROM `user_used_characters` WHERE `user_id` = :user_id");
	  $userStmt->execute(['user_id' => $user_id]);
	  $used_character = $userStmt->fetch()['used_character'];
	
	
	 $select_1 = $this->db->prepare("SELECT * FROM `users` WHERE `id` = :user_id");
			$select_1->execute([
				'user_id' => $user_id
			]);
			$row = $select_1->fetch();
			$purchased_char = $row['purchased_char'];
		
	  $userStmt = $this->db->prepare("SELECT value as total_character_limit FROM `settings` WHERE type = 14 AND `object` = :object");
	  $userStmt->execute(['object' => $object]);
	  $total_character_limit = $userStmt->fetch()['total_character_limit'];
	  $total_character_limit = $total_character_limit + $purchased_char;
	  $left_percentage = number_format(((((int)$total_character_limit- (int)$used_character))/$total_character_limit)*100,1);
	  $used_percentage = number_format((((int)$used_character)/$total_character_limit)*100,1);
	   return array( 'total_char' => (int)$total_character_limit , 'left_character' => (int)$total_character_limit- (int)$used_character ,'left_percentage' => $left_percentage ,'used_percentage' => $used_percentage);
	
}//function
	function updateCharacterLimit( $user_id , $max_characters , $characters_code ){
		
		$select = $this->db->prepare("SELECT * FROM `user_code` WHERE `user_id` = :user_id and `code`= :code");
		$select->execute([
			'user_id' => $user_id,
			'code' => $characters_code
    	]);
		$existing = $select->fetch();
		if ( !is_array( $existing ) ) {
			 $insert = $this->db->prepare("INSERT INTO `user_code` (`user_id`, `code`) VALUES (:user_id, :code)");
			 $insert->execute([
				'user_id' => $user_id,
				'code' => $characters_code
			  ]);
			$select_1 = $this->db->prepare("SELECT * FROM `users` WHERE `id` = :user_id");
			$select_1->execute([
				'user_id' => $user_id
			]);
			$row = $select_1->fetch();
			$purchased_char = $row['purchased_char'];
			$purchased_char = $purchased_char+$max_characters;
      		$update = $this->db->prepare("UPDATE `users` SET `purchased_char` = :purchased_char WHERE `id` = :id");
			$update->execute([
				'purchased_char' => $purchased_char,
				'id' => $user_id
			]);
			return true;
		}else{
			return false;
		} 
	}
    public function getTotalCount()
    {
        $query =  $this->db->prepare("SELECT count(*) as total FROM ".$this->getTableName() );
        $query->execute();
        
        $result = $query->fetch();
        return $result['total'];
        
    }
    
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}    
}
