<?php
namespace spir1donov;

use function Couchbase\defaultDecoder;
use Sart\User;
use DateTime;
use DateInterval;

class Authentication {
  /** @var \PDO $db */
  
  private static $_instance = null;
  
  private $db;

  private $user;

  private $cookieName = 'appToken';

  function __construct(\PDO $db) {
    $this->db = $db;

    return $this->authenticate();
  }

  function authenticate() {
    $user = new User();

    if(isset($_COOKIE[$this->cookieName])) {
        $user->findByAttributes('token',$_COOKIE[$this->cookieName]);    
    }
    
    $this->user = $user;
    $this->user->limits = $user->calcLimits($user);

    if (isset($user->id)) {
      $stmt = $this->db->prepare('UPDATE users SET last_active = NOW() WHERE id = :id');
      $stmt->execute(['id' => $user->id]);
    }

    return $this->user;
  }

  function getLimits() {
      return $this->user->limits;
  }

  function saveAccountLimits($limits) {
      if(isset($limits['video']['basic']['per_hour']['limit'])) {
          var_dump($limits['video']['basic']['per_hour']['limit']);
          $sql = "UPDATE `settings` SET `value` = :val WHERE `type`=10 AND `object`=0 AND `key`='video_per_hour'";
          $userStmt = $this->db->prepare($sql);
          $userStmt->execute(['val' => $limits['video']['basic']['per_hour']['limit']]);
      }
      if(isset($limits['video']['pro']['per_hour']['limit'])) {
          $sql = "UPDATE `settings` SET `value` = :val WHERE `type`=10 AND `object`=1 AND `key`='video_per_hour'";
          $userStmt = $this->db->prepare($sql);
          $userStmt->execute(['val' => $limits['video']['pro']['per_hour']['limit']]);
      }
      if(isset($limits['video']['pro_plus']['per_hour']['limit'])) {
          $sql = "UPDATE `settings` SET `value` = :val WHERE `type`=10 AND `object`=3 AND `key`='video_per_hour'";
          $userStmt = $this->db->prepare($sql);
          $userStmt->execute(['val' => $limits['video']['pro_plus']['per_hour']['limit']]);
      }
      if(isset($limits['video']['basic']['total']['limit'])) {
          $sql = "UPDATE `settings` SET `value` = :val WHERE `type`=10 AND `object`=0 AND `key`='video_total'";
          $userStmt = $this->db->prepare($sql);
          $userStmt->execute(['val' => $limits['video']['basic']['total']['limit']]);
      }
      if(isset($limits['video']['pro']['total']['limit'])) {
          $sql = "UPDATE `settings` SET `value` = :val WHERE `type`=10 AND `object`=1 AND `key`='video_total'";
          $userStmt = $this->db->prepare($sql);
          $userStmt->execute(['val' => $limits['video']['pro']['total']['limit']]);
      }
      if(isset($limits['video']['pro_plus']['total']['limit'])) {
          $sql = "UPDATE `settings` SET `value` = :val WHERE `type`=10 AND `object`=3 AND `key`='video_total'";
          $userStmt = $this->db->prepare($sql);
          $userStmt->execute(['val' => $limits['video']['pro_plus']['total']['limit']]);
      }
      if(isset($limits['diskspace']['basic']['limit'])) {
          $sql = "UPDATE `settings` SET `value` = :val WHERE `type`=10 AND `object`=0 AND `key`='diskspace'";
          $userStmt = $this->db->prepare($sql);
          $userStmt->execute(['val' => $limits['diskspace']['basic']['limit']]);
      }
      if(isset($limits['diskspace']['pro']['limit'])) {
          $sql = "UPDATE `settings` SET `value` = :val WHERE `type`=10 AND `object`=1 AND `key`='diskspace'";
          $userStmt = $this->db->prepare($sql);
          $userStmt->execute(['val' => $limits['diskspace']['pro']['limit']]);
      }
      if(isset($limits['diskspace']['pro_plus']['limit'])) {
          $sql = "UPDATE `settings` SET `value` = :val WHERE `type`=10 AND `object`=3 AND `key`='diskspace'";
          $userStmt = $this->db->prepare($sql);
          $userStmt->execute(['val' => $limits['diskspace']['pro_plus']['limit']]);
      }
      if(isset($limits['slides']['basic']['limit'])) {
          $sql = "UPDATE `settings` SET `value` = :val WHERE `type`=11 AND `object`=0 AND `key`='slides'";
          $userStmt = $this->db->prepare($sql);
          $userStmt->execute(['val' => $limits['slides']['basic']['limit']]);
      }
      if(isset($limits['slides']['pro']['limit'])) {
          $sql = "UPDATE `settings` SET `value` = :val WHERE `type`=11 AND `object`=1 AND `key`='slides'";
          $userStmt = $this->db->prepare($sql);
          $userStmt->execute(['val' => $limits['slides']['pro']['limit']]);
      }
      if(isset($limits['slides']['pro_plus']['limit'])) {
          $sql = "UPDATE `settings` SET `value` = :val WHERE `type`=11 AND `object`=3 AND `key`='slides'";
          $userStmt = $this->db->prepare($sql);
          $userStmt->execute(['val' => $limits['slides']['pro_plus']['limit']]);
      }

      if(isset($limits['background_images']['basic']['limit'])) {
          $sql = "UPDATE `settings` SET `value` = :val WHERE `type`=12 AND `object`=0 AND `key`='background_images'";
          $userStmt = $this->db->prepare($sql);
          $userStmt->execute(['val' => $limits['background_images']['basic']['limit']]);
      }
      if(isset($limits['background_images']['pro']['limit'])) {
          $sql = "UPDATE `settings` SET `value` = :val WHERE `type`=12 AND `object`=1 AND `key`='background_images'";
          $userStmt = $this->db->prepare($sql);
          $userStmt->execute(['val' => $limits['background_images']['pro']['limit']]);
      }
      if(isset($limits['background_images']['pro_plus']['limit'])) {
          $sql = "UPDATE `settings` SET `value` = :val WHERE `type`=12 AND `object`=1 AND `key`='background_images'";
          $userStmt = $this->db->prepare($sql);
          $userStmt->execute(['val' => $limits['background_images']['pro_plus']['limit']]);
      }

      if(isset($limits['music_files']['basic']['limit'])) {
          $sql = "UPDATE `settings` SET `value` = :val WHERE `type`=13 AND `object`=0 AND `key`='music_files'";
          $userStmt = $this->db->prepare($sql);
          $userStmt->execute(['val' => $limits['music_files']['basic']['limit']]);
      }
      if(isset($limits['music_files']['pro']['limit'])) {
          $sql = "UPDATE `settings` SET `value` = :val WHERE `type`=13 AND `object`=1 AND `key`='music_files'";
          $userStmt = $this->db->prepare($sql);
          $userStmt->execute(['val' => $limits['music_files']['pro']['limit']]);
      }
      if(isset($limits['music_files']['pro_plus']['limit'])) {
          $sql = "UPDATE `settings` SET `value` = :val WHERE `type`=13 AND `object`=1 AND `key`='music_files'";
          $userStmt = $this->db->prepare($sql);
          $userStmt->execute(['val' => $limits['music_files']['pro_plus']['limit']]);
      }

	   if(isset($limits['character_limit']['basic']['limit'])) {
          $sql = "UPDATE `settings` SET `value` = :val WHERE `type`=14 AND `object`=0 AND `key`='character_limit'";
          $userStmt = $this->db->prepare($sql);
          $userStmt->execute(['val' => $limits['character_limit']['basic']['limit']]);
      }
      if(isset($limits['character_limit']['pro']['limit'])) {
          $sql = "UPDATE `settings` SET `value` = :val WHERE `type`=14 AND `object`=1 AND `key`='character_limit'";
          $userStmt = $this->db->prepare($sql);
          $userStmt->execute(['val' => $limits['character_limit']['pro']['limit']]);
      }
      if(isset($limits['character_limit']['pro_plus']['limit'])) {
          $sql = "UPDATE `settings` SET `value` = :val WHERE `type`=14 AND `object`=3 AND `key`='character_limit'";
          $userStmt = $this->db->prepare($sql);
          $userStmt->execute(['val' => $limits['character_limit']['pro_plus']['limit']]);
      }
	  
      return $this->recalcLimits($this->user);
  }

  function recalcLimits($user) {
      if(isset($user->id)) {
          $this->user->limits = $user->calcLimits($user);
          return $this->user->limits;
      }
      return null;
  }

  function login() {
    $user = User::model()->findByAttributes([
        'email'=>$_POST['email'],
        'password'=>md5($_POST['password'])
    ]);
    
    $this->user = $user;

    if(isset($user->id)) {
        $attributes = $this->user->getAttributes();
        if (isset($attributes['lastActive'])) {
          $lastActive = new DateTime($attributes['lastActive']);
          $now = new DateTime();
          $diff = $now->diff($lastActive);
          $hours = $diff->h;
          $hours += $diff->days*24;
          if ($hours < 3) {
            return array(
              'user' => new User(),
              'message' => 'Account being used - Access Denied',
            );
          }
        }
        $token = md5(microtime(true) . implode(' ', $this->user->getAttributes()));
        $user->updateAttribute('token', $token);
        setcookie($this->cookieName, $token, 0, '/');
        $this->recalcLimits($user);
        $this->recalcVIPSubscription($user);
        return array(
          'user' => $this->user,
          'message' => '',
        );
    }

    return array(
      'user' => $this->user,
      'message' => 'Invalid email and/or password',
    );
  }

  function recalcVIPSubscription($user) {
      $days_to_upgrade_month = 30;
      if ($user->license_vip && $user->email) { // vip license are present

          if ($this->verifyLicense($user->email, $user->license_vip, 'vip')) { // validation
          //if ($this->verifyLicense('314963', 'sketch-maker-pro', 'test6@email.com', $user->license_vip)) { // validation
              $userStmt = $this->db->prepare("UPDATE `users` SET `users`.account_type=3 WHERE `id` = :id");
              $userStmt->execute(['id' => $user->id]);
              $user->account_type = '3';

              if (!$user->license_vip_startdate) {
                  $userStmt = $this->db->prepare("UPDATE `users` SET `users`.license_vip_startdate=NOW(), `users`.license_vip_lastcheckup=NOW() WHERE `id` = :id");
                  $userStmt->execute(['id' => $user->id]);
                  $user->license_vip_startdate = date("Y-m-d");
                  $user->license_vip_lastcheckup = date("Y-m-d");
              } else {
                  if ($user->license_vip_lastcheckup != date("Y-m-d")) { // last checkup not today
                      $userStmt = $this->db->prepare("UPDATE `users` SET `users`.license_vip_lastcheckup=NOW() WHERE `id` = :id");
                      $userStmt->execute(['id' => $user->id]);
                      $user->license_vip_lastcheckup = date("Y-m-d");

                      $userStmt = $this->db->prepare("SELECT DATEDIFF(`users`.license_vip_lastcheckup, `users`.license_vip_startdate) as `days` FROM `users` WHERE `id` = :id");
                      $userStmt->execute(['id' => $user->id]);
                      $days = $userStmt->fetch()['days'];
                      if ( $days > $days_to_upgrade_month ) { // user got new months!
                          $userStmt = $this->db->prepare("UPDATE `users` SET `users`.license_vip_startdate=DATE_ADD(`users`.license_vip_startdate, INTERVAL " . (intval($days / $days_to_upgrade_month) * $days_to_upgrade_month) . " DAY ), `users`.license_vip_total_months_count = `users`.license_vip_total_months_count + " . intval($days / $days_to_upgrade_month) . " WHERE `id` = :id");
                          $userStmt->execute(['id' => $user->id]);
                      }
                  }
              }
          } else { // license expired
              // remove data counts and fall back account type to user license type
              $userStmt = $this->db->prepare("UPDATE `users` SET `users`.license_vip_startdate = NULL, `users`.license_vip_lastcheckup = NULL, `users`.license_vip = NULL, `users`.account_type = :account_type WHERE `id` = :id");
              $userStmt->execute([
                  'id' => $user->id,
                  'account_type' => $this->verifyLicense($user->email, $user->license, 'pro') ? 1 : 0
              ]);
          }
      }
  }

    /*
     *  PRO PLUS: '328680', 'sketch-maker-pro-plus-(m2)'
     *  VIP:    '319485', 'sketch-maker-pro-plus-vip'
     *  PRO:    '314963', 'sketch-maker-pro'
     *  BASIC:  '318057', 'sketch-maker-basic'
     */
    function verifyLicense($email, $license, $type = "basic") {
        switch ($type) {
            case "pro-plus-m2":
                $pn = '328682';
                $slug = 'sketch-maker-pro-plus-(m2)';
                break;

            case "pro":
                $pn = '314963';
                $slug = 'sketch-maker-pro';
                break;

            case "vip": // http://softwarelicensing.pro/licenses/a?action=check_license&license=SL-774290101017LACX&email=vip1@email.com&product_number=319485&slug=sketch-maker-pro-plus-vip
                $pn = '319485';
                $slug = 'sketch-maker-pro-plus-vip';
                break;

            case "basic":
            default:
                $pn = '318057';
                $slug = 'sketch-maker-basic';
        }

        try {
            $ch = curl_init();
            // Set URL to download
            $url = 'http://www.softwarelicensing.pro/licenses/a?action=check_license&product_number=' . $pn . '&slug=' . $slug . '&license=' . $license . '&email=' . $email;
            curl_setopt($ch, CURLOPT_URL, $url);
            // Include header in result? (0 = yes, 1 = no)
            curl_setopt($ch, CURLOPT_HEADER, 0);
            //Set content Type
            //curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-type: application/json'));
            // Should cURL return or print out the data? (true = return, false = print)
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            //to blindly accept any server certificate, without doing any verification //optional
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            //The most important is
            curl_setopt($ch, CURLOPT_SSLVERSION, 3);
            // Download the given URL, and return output
            $result = curl_exec($ch);
            // Close the cURL resource, and free system resources
            curl_close($ch);

            $data = json_decode($result, true);
            if (isset($data['status'])) {
                if ($data['status'] === 'valid') {
                    return true;
                }
            }
        } catch (Exception $exception) {
            if ($type == "vip") {
                return $this->verifyLicense($email, $license, "pro-plus-m2");
            }
            return false;
        }
        if ($type == "vip") {
            return $this->verifyLicense($email, $license, "pro-plus-m2");
        }
        return false;
    }


  function logout() {
    if (isset($this->user->id)) {
      $stmt = $this->db->prepare('UPDATE users SET last_active = NULL WHERE id = :id');
      $stmt->execute(['id' => $this->user->id]);
    }
    setcookie($this->cookieName, '');
  }

  

    public static function getInstance()
    {
        if(!self::$_instance)
        {
            self::$_instance = new Authentication(\spir1donov\Database::getDb());
        }
        
        return self::$_instance;
    }

    
  
    /**
     * return user data
     */ 
    function getUser()
    {
      return $this->user;
    }

    /**
     * @todo: Refactoring required, should be moved into the user class
     * @param $email
     * @param $password
     * @param $license
     * @param $account_type
     * @return bool
     */

  function addUser($email, $password, $license, $account_type) {
    $userStmt = $this->db->prepare("INSERT INTO `users` (`email`, `password`, `addedOn`,`license`,`license_vip`, `account_type`) VALUES (:email, :password, NOW(), :license, :license, :account_type)");
    return $userStmt->execute([
        'email' => $email,
        'password' => md5($password),
        'license' => $license,
        'account_type' => $account_type,
    ]);
  }

  function editUser() {
    $userStmt = $this->db->prepare("UPDATE `users` SET `email` = :email, `password` = :password WHERE `id` = :id");
    return $userStmt->execute([
        'id' => $_POST['id'],
        'email' => $_POST['email'],
        'password' => md5($_POST['password']),
    ]);
  }

  function deleteUser() {
    $userStmt = $this->db->prepare("DELETE FROM `users` WHERE `id` = :id");
    return $userStmt->execute([
      'id' => $_POST['id']
    ]);
  }

  function getUsersList() {
   
    $where = [];
    $values =[];
    if(!empty($_POST['license'])) {
        $where[] = ' `license` LIKE :license';
        $values['license'] = '%'.$_POST['license'].'%';
    }
    if(!empty($_POST['email'])) {
        $where[] = ' `email` LIKE :email';
        $values['email'] = '%'.$_POST['email'].'%';
    }
    $where = count($where) > 0 ? 'WHERE ' . implode(' AND ',$where) : '';
    $listStmt = $this->db->prepare("SELECT `id`,`email`,`license`,`account_type`,`addedOn`, `isAdmin`,`old_account_type` FROM `users` $where");
    $listStmt->execute($values);
    return $listStmt->fetchAll();
  }
}
